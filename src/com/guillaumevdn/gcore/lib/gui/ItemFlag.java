package com.guillaumevdn.gcore.lib.gui;

/**
 * @author GuillaumeVDN
 */
public enum ItemFlag {

	HIDE_ATTRIBUTES,
	HIDE_DESTROYS,
	HIDE_ENCHANTS,
	HIDE_PLACED_ON,
	HIDE_POTION_EFFECTS,
	HIDE_DYE,
	HIDE_UNBREAKABLE;

}
