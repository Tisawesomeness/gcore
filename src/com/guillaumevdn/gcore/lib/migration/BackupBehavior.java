package com.guillaumevdn.gcore.lib.migration;

/**
 * @author GuillaumeVDN
 */
public enum BackupBehavior {

	NONE,
	RESTORE,
	DELETE;

}
