package com.guillaumevdn.gcore.lib.data;

/**
 * @author GuillaumeVDN
 */
public enum DataBackEnd {

	NONE,
	JSON,
	SQLITE,
	MYSQL;

}
