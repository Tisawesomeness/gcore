package com.guillaumevdn.gcore.lib.legacy_npc.navigation.pathfinding;

/**
 * @author GuillaumeVDN
 */
public enum PathfindingResult {

	SUCCESS,
	FAILURE_NO_MORE_POINTS,
	CANCEL
	;

}
