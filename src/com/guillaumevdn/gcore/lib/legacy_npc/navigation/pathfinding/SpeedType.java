package com.guillaumevdn.gcore.lib.legacy_npc.navigation.pathfinding;

/**
 * @author GuillaumeVDN
 */
public enum SpeedType {

	WALK,
	SPRINT;

}
