package com.guillaumevdn.gcore.lib.legacy_npc.navigation;

/**
 * @author GuillaumeVDN
 */
public class NPCConfig {

	public static final long pathfindingMaxMillisPerTick = 30L;
	public static final long navigationTimeoutTicks = 100L;
	public static final long navigationBatchMovementsRecheckMillis = 2500L;

}
