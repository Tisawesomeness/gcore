package com.guillaumevdn.gcore.lib.legacy_npc.navigation.navigator;

/**
 * @author GuillaumeVDN
 */
public enum NavigatorResult {

	SUCCESS,
	FAILURE_NO_PATH_FOUND,
	FAILURE_TIMEOUT,
	CANCEL
	;

}
