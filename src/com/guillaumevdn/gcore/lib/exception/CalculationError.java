package com.guillaumevdn.gcore.lib.exception;

/**
 * @author GuillaumeVDN
 */
public class CalculationError extends Error {

	private static final long serialVersionUID = 4723673620719545559L;

	public CalculationError(String message) {
		super(message);
	}

}
