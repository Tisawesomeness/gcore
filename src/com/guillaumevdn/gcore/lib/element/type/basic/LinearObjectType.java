package com.guillaumevdn.gcore.lib.element.type.basic;

import com.guillaumevdn.gcore.lib.compatibility.material.Mat;

/**
 * @author GuillaumeVDN
 */
public interface LinearObjectType {

	String name();
	boolean requireParam();
	Mat getIcon();

}
