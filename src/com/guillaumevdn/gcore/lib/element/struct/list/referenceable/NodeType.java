package com.guillaumevdn.gcore.lib.element.struct.list.referenceable;

/**
 * @author GuillaumeVDN
 */
public enum NodeType {

	GLOBAL, LOCAL;

}
