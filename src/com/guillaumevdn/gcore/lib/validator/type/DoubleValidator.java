package com.guillaumevdn.gcore.lib.validator.type;

import com.guillaumevdn.gcore.lib.validator.Validator;

/**
 * @author GuillaumeVDN
 */
public abstract class DoubleValidator extends Validator<Double> {

	public DoubleValidator(String mustBeDescription) {
		super(mustBeDescription);
	}

}
