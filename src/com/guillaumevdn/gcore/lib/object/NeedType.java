package com.guillaumevdn.gcore.lib.object;

/**
 * @author GuillaumeVDN
 */
public enum NeedType {

	REQUIRED,
	OPTIONAL
	;

}
