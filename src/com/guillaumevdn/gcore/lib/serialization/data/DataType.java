package com.guillaumevdn.gcore.lib.serialization.data;

/**
 * @author GuillaumeVDN
 */
public enum DataType {

	OBJECT,
	LIST,
	VALUE;

}
