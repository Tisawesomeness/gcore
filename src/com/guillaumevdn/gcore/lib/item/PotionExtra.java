package com.guillaumevdn.gcore.lib.item;

/**
 * @author GuillaumeVDN
 */
public enum PotionExtra {

	EXTENDED,
	UPGRADED,
	NONE;

}
