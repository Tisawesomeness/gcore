package com.guillaumevdn.gcore.lib.location.position;

/**
 * @author GuillaumeVDN
 */
public enum PositionNeed {

	REQUIRED,
	OPTIONAL,
	FORBIDDEN;

}
