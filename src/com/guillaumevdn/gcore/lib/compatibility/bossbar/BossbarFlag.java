package com.guillaumevdn.gcore.lib.compatibility.bossbar;

/**
 * @author GuillaumeVDN
 */
public enum BossbarFlag {

	CREATE_FOG,
	DARKEN_SKY,
	PLAY_BOSS_MUSIC;

}
