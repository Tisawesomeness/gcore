package com.guillaumevdn.gcore.lib.compatibility.bossbar;

/**
 * @author GuillaumeVDN
 */
public enum BossbarColor {

	BLUE,
	GREEN,
	PINK,
	PURPLE,
	RED,
	WHITE,
	YELLOW;

}
