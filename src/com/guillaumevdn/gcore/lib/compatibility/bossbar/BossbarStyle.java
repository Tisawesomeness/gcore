package com.guillaumevdn.gcore.lib.compatibility.bossbar;

/**
 * @author GuillaumeVDN
 */
public enum BossbarStyle {

	SOLID,
	SEGMENTED_6,
	SEGMENTED_10,
	SEGMENTED_12,
	SEGMENTED_20

	// ----- spigot names ?
	// ----- PROGRESS
	// ----- NOTCHED_6
	// ----- NOTCHED_10
	// ----- NOTCHED_12
	// ----- NOTCHED_20

}
